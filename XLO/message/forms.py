from django import forms

from .models import Message
from ads.models import Advertisement

class MessageForm(forms.ModelForm):
	#advert = forms.ModelChoiceField(label='', queryset=Advertisement.objects.all(), widget=forms.HiddenInput())

	class Meta:
		model = Message
		fields = ['msg_body']
			