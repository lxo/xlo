# coding=utf-8

from django.db import models

# Create your models here.
class Message(models.Model):

    sender = models.ForeignKey('profiles.UserProfile', 
        verbose_name = "Nadawca", related_name='sent_messages')
    recipient = models.ForeignKey('profiles.UserProfile', related_name='received_messages')
    thread = models.ForeignKey('Thread', related_name='thread_messages', verbose_name='Wątek')
    msg_body = models.TextField(verbose_name='Treść Wiadomości:')
    send_date = models.DateTimeField(auto_now_add=True, verbose_name='Data wysłania')

    class Meta:
        verbose_name = "Wiadomość"
        verbose_name_plural = "Wiadomości"

    def __unicode__(self):
        return '[' + self.thread.advert.title + '] ' + self.msg_body[:15] + '...]' 



class Thread(models.Model):
    buyer = models.ForeignKey('profiles.UserProfile', related_name='buyer_threads')
    seller = models.ForeignKey('profiles.UserProfile', related_name='seller_threads')
    advert = models.ForeignKey('ads.Advertisement', verbose_name='Ogłoszenie')
    
    class Meta: 
        verbose_name = "Wątek"
        verbose_name_plural = "Wątki"

    def __unicode__(self):
        return str(self.id) + self.advert.title 
    