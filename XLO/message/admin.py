from django.contrib import admin

from .forms import MessageForm
from .models import Message, Thread

class MessageAdmin(admin.ModelAdmin):
	form = MessageForm


admin.site.register(Message)
admin.site.register(Thread)