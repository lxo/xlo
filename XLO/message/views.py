from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect

from profiles.models import UserProfile
from ads.models import Advertisement

from .forms import MessageForm
from .models import Message, Thread

@login_required
def list_view(request):
	user_logged_in = request.user.userprofile
	t_list = user_logged_in.buyer_threads.all()
	t_list = t_list | user_logged_in.seller_threads.all()

	context = {'messages_list': t_list}
	return render(request, 'message/list.html', context)

@login_required
def view_thread(request, thread_id):
	thread = Thread.objects.get(id=thread_id)
	thread = thread.thread_messages.all()

	context = {'thread' : thread}
	context['contact_form'] = MessageForm()
	return render(request, 'message/thread.html', context)

@login_required
def create_message(request):
	
	if request.POST:
		ad = Advertisement.objects.get(id = request.POST['advert'])
		form = MessageForm(request.POST)
		if form.is_valid():
			obj=form.save(commit=False)
			#walidacja
			obj.sender=request.user.userprofile
			obj.recipient=ad.profile
			#watek
			thread_q = Thread.objects.filter(advert=ad)
			thread_q = thread_q.filter(seller=obj.sender, buyer=obj.recipient) | thread_q.filter(seller=obj.recipient, buyer=obj.sender)
			try:
				thread = thread_q[0:1].get()
			except Thread.DoesNotExist:
				thread = Thread(seller = obj.recipient ,buyer = obj.sender ,advert = ad)
				thread.save()

			obj.thread = thread
			obj.save()

		return HttpResponseRedirect('/ads/%s/details' %thread.advert.pk)


	#nieuzywany kod
	context = {'contact_form' : MessageForm}
	return render(request, 'message/create.html', context)
	
@login_required
def reply_message(request):
	if request.POST:
		thread = Thread.objects.get(id = request.POST['thread'])
		print thread
		form = MessageForm(request.POST)
		if 1: #form.is_valid():
			obj=form.save(commit=False)
			#walidacja
			obj.sender=request.user.userprofile
			if thread.seller == obj.sender:
				obj.recipient = thread.buyer
			else:
				obj.recipient = thread.seller

			obj.thread = thread
			print obj
			obj.save()
		else:
			print "ERROR"
		return HttpResponseRedirect('/message/%s/' %thread.pk)
