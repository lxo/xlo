from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.list_view ,name="messages_list"),
    url(r'^(?P<thread_id>[0-9]+)/$', views.view_thread, name='details'),
    url(r'^create_message$', views.create_message, name='create_message'),
    url(r'^reply_message$', views.reply_message, name='reply_message'),
]