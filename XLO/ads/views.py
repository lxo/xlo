from django.http import HttpResponse
from django.shortcuts import render,redirect
from django.contrib.auth.decorators import login_required

from .models import Advertisement
from .forms import AdvertisementForm

from message.forms import MessageForm

def details(request, ad_id):
	#profile = request.user.userprofile
	ad=Advertisement.objects.get(id=ad_id)
	context = {'ad' : ad}
	
	context['contact_form'] = MessageForm()
	return render(request, 'ads/details.html', context)

@login_required         
def create(request):
	profile = request.user.userprofile

	if request.POST:
		ad_form = AdvertisementForm(request.POST or None, request.FILES)
		if ad_form.is_valid():
			adver = ad_form.save(commit=False)
			adver.profile = profile
			adver.save()
			return redirect(profile)
	else:
		ad_form = AdvertisementForm()

	context_dict = {'form': ad_form}
	return render(request, 'ads/create.html', context_dict)


def all_ads(request):
    ads_list = Advertisement.objects.filter(active=True)
    context =  {'ads_list': ads_list}
    return render(request, 'ads/list.html', context)



def edit(request, ad_id):
	profile = request.user.userprofile
	ad=Advertisement.objects.get(id=ad_id)
	if request.POST:
		ad_form = AdvertisementForm(request.POST, request.FILES, instance=ad)
		if ad_form.is_valid():
			ad_form.save()
			return redirect(ad)
	else:
		ad_form = AdvertisementForm(instance = ad)
	context = {'ad' : ad, 'form' : ad_form}
	return render(request, 'ads/edit.html', context)
