# coding=utf-8
from django.db import models
from django.core.urlresolvers import reverse
# Create your models here.

class Advertisement(models.Model):
    CATEGORIES = (
        ("Art sp","Artykuly spozywcze"),
        ("RTV AGD","RTV AGD"),
        ("inne","inne")
    )
    title = models.CharField(verbose_name = 'Tytuł ogłoszenia',
                             max_length = 70)
    price = models.CharField(max_length=20)
    content = models.TextField(verbose_name = 'Treść ogłoszenia')

    photo = models.ImageField(verbose_name='Zdjęcie',
                               blank = True, 
                               null = True,
                               upload_to='ogloszenia/photos')

    profile = models.ForeignKey('profiles.UserProfile')
    create_date = models.DateField(auto_now_add=True)
    category = models.CharField(verbose_name = 'Kategoria',
                                     max_length=20, 
                                     choices=CATEGORIES) 
    active = models.BooleanField(default=False, verbose_name='Czy aktywne?')
    def __unicode__(self):
    	return self.title

    def get_absolute_url(self):
        return reverse('ads.views.details', kwargs={'ad_id': self.id})

    def get_absolute_edit_url(self):
        return reverse('ads.views.edit', kwargs={'ad_id': self.id})

    def get_photo_url(self):
        if self.photo and hasattr(self.photo, 'url'):
            return self.photo.url
        else:
            return '/static/images/no_photo.png'