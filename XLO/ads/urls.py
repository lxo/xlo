from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.all_ads,name="all_ads"),
    url(r'^(?P<ad_id>[0-9]+)/details/$', views.details,name='ads_details'),
    url(r'^create/$', views.create,name="ads_create"),
    url(r'^(?P<ad_id>[0-9]+)/edit/$', views.edit,name='ads_edit'),    
]
