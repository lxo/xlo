from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required

from .forms import UserProfileForm

from django.contrib.auth.models import User
from .models import UserProfile
from ads.models import Advertisement

def list_view(request):
	profiles = UserProfile.objects.all()
	context = {'profiles': profiles}
	return render(request, 'profiles/list.html', context)

@login_required
def myprofile_view(request):
    profile = request.user.userprofile

    if request.POST:
        profile_form = UserProfileForm(request.POST, request.FILES, instance=profile)
        if profile_form.is_valid():
            profile_form.save()
            return redirect(profile)
    else:
        profile_form = UserProfileForm(instance = profile)


    context_dict = {'profile': profile, 'form': profile_form}

    return render(request, 'profiles/myprofile.html', context_dict)

def detail_view(request, user_name):
    profile = get_object_or_404(User, username=user_name).userprofile
    ads_list = Advertisement.objects.filter(profile=profile)
    context = {'profile': profile, 'ads_list': ads_list}

    return render(request, 'profiles/details.html', context)
