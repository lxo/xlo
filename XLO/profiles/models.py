# coding=utf-8

from django.db import models

from django.db.models.signals import post_save
from django.core.urlresolvers import reverse

from django.contrib.auth.models import User

# User Profile class extending django.contrib.auth.models.User
class UserProfile(models.Model):

    DORM_CHOICES = (
      ('mikrus', 'Mikrus'),
      ('ustronie', 'Ustronie'),
      ('riviera', 'Riviera'),
    )
    dorm = models.CharField(verbose_name = 'Akademik',
                            max_length = 16,
                            choices=DORM_CHOICES)

    avatar = models.ImageField(verbose_name='Avatar',
                               blank = True, 
                               null = True,
                               upload_to='profiles/avatars')

    name = models.CharField(verbose_name = 'Imię i nazwisko',
                            max_length = 32,
                            blank = True,
                            null = True)

    phone = models.CharField(verbose_name = 'Nr telefonu',
                             max_length = 9,
                             blank = True,
                             null = True)

    user = models.OneToOneField(User)


    class Meta:
      verbose_name_plural = 'Profile użytkowników'

    def __str__(self):
        return self.user.username

    def get_absolute_url(self):
        return reverse('profiles.views.detail_view', kwargs={'user_name': self.user.username})

    def get_avatar_url(self):
        if self.avatar and hasattr(self.avatar, 'url'):
            return self.avatar.url
        else:
            return '/static/images/default_profile_image.png'

    

# Creates User Profile (when new user instance is created) by post_save signal
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        profile, created = UserProfile.objects.get_or_create(user=instance)

# link registering new users with creating profiles for them
post_save.connect(create_user_profile, sender=User)