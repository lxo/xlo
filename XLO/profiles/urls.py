from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.list_view, name='profiles_list'),
    url(r'^ja/$', views.myprofile_view, name='profiles_myprofile'),
    url(r'^(?P<user_name>[\w@.+-_]+)/$', views.detail_view, name='profiles_detail'),
]